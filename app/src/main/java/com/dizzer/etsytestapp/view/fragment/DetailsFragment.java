package com.dizzer.etsytestapp.view.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.text.HtmlCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dizzer.etsytestapp.R;
import com.dizzer.etsytestapp.di.component.ActivityComponent;
import com.dizzer.etsytestapp.presenter.impl.DetailsFragmentPresenter;
import com.dizzer.etsytestapp.view.base.BaseActivity;
import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.contract.DetailsFragmentView;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DetailsFragment extends BaseFragment implements DetailsFragmentView, View.OnClickListener {

    Unbinder unbinder;
    @BindView(R.id.imageDetailFragment)
    ImageView imageDetailFragment;
    @BindView(R.id.titleDetailFragment)
    TextView titleDetailFragment;
    @BindView(R.id.priceDetailFragment)
    TextView priceDetailFragment;
    @BindView(R.id.descriptionsDetailFragment)
    TextView descriptionsDetailFragment;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private BaseActivity mActivity;
    private static final String LISTING_ID = "id";
    private static final String LISTING_TITLE = "title";
    private static final String LISTING_PRICE = "price";
    private static final String LISTING_DESCRIPTION = "description";
    private static final String LISTING_IMAGE_URL_FULL = "imageUrlFull";
    private static final String LISTING_IMAGE_URL_SHORT = "imageUrlShort";
    private static final String LISTING_IS_SAVED = "isSaved";

    @Inject
    DetailsFragmentPresenter presenter;

    private boolean isSaved;
    private Long id;
    private String title, price, description, imageUrlFull, imageUrlShort;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listing_details_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        performDependencyInjection();
        presenter.setView(this);
        getArgumentsData();
        presenter.checkItem(id);
        setFragment();
        fab.setOnClickListener(this);
        return view;
    }

    private void setFragment() {
        if (isSaved){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp_active, getActivity().getTheme()));
            } else {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp_active));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp, getActivity().getTheme()));
            } else {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp));
            }
        }
        Picasso.get().load(imageUrlFull).into(imageDetailFragment);
        titleDetailFragment.setText(HtmlCompat.fromHtml(title,
                HtmlCompat.FROM_HTML_MODE_LEGACY));
        priceDetailFragment.setText("$ " + price);
        descriptionsDetailFragment.setText(description);
//      Different format
//        descriptionsDetailFragment.setText(HtmlCompat.fromHtml(description,
//                HtmlCompat.FROM_HTML_MODE_LEGACY));
    }

    @Override
    protected int getLayout() {
        return R.layout.listing_details_fragment;
    }

    @Override
    public void performDependencyInjection() {
        ActivityComponent component = getActivityComponent();
        if (getActivityComponent() != null) {
            component.inject(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
        }
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null) {
            return mActivity.getActivityComponent();
        }
        return null;
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }

    public static Fragment newInstance(Long id, String title, String price, String description,
                                       String imageUrlFull, String imageUrlShort, boolean isSaved) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putLong(LISTING_ID, id);
        args.putString(LISTING_TITLE, title);
        args.putString(LISTING_PRICE, price);
        args.putString(LISTING_DESCRIPTION, description);
        args.putString(LISTING_IMAGE_URL_FULL, imageUrlFull);
        args.putString(LISTING_IMAGE_URL_SHORT, imageUrlShort);
        args.putBoolean(LISTING_IS_SAVED, isSaved);
        fragment.setArguments(args);
        return fragment;
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            id = getArguments().getLong(LISTING_ID);
            title = getArguments().getString(LISTING_TITLE);
            price = getArguments().getString(LISTING_PRICE);
            description = getArguments().getString(LISTING_DESCRIPTION);
            imageUrlFull = getArguments().getString(LISTING_IMAGE_URL_FULL);
            imageUrlShort = getArguments().getString(LISTING_IMAGE_URL_SHORT);
            isSaved = getArguments().getBoolean(LISTING_IS_SAVED);
        }
    }

    @Override
    public void onClick(View view) {
        if (isSaved){
            isSaved = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp, getActivity().getTheme()));
            } else {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp));
            }
        } else {
            isSaved = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp_active, getActivity().getTheme()));
            } else {
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_24dp_active));
            }
        }
        presenter.onSaveClick(id, title, price, description, imageUrlFull, imageUrlShort, isSaved);
    }

    @Override
    public void setIsSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }
}
