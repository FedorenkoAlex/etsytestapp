package com.dizzer.etsytestapp.di.component;

import com.dizzer.etsytestapp.di.PerActivity;
import com.dizzer.etsytestapp.di.module.ActivityModule;
import com.dizzer.etsytestapp.view.MainActivity;
import com.dizzer.etsytestapp.view.fragment.DetailsFragment;
import com.dizzer.etsytestapp.view.fragment.MainScreenFragment;
import com.dizzer.etsytestapp.view.fragment.SavedFragment;
import com.dizzer.etsytestapp.view.fragment.SearchFragment;
import com.dizzer.etsytestapp.view.fragment.SearchResultFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity activity);
    void inject(SearchFragment fragment);
    void inject(SearchResultFragment fragment);
    void inject(MainScreenFragment fragment);
    void inject(DetailsFragment fragment);
    void inject(SavedFragment fragment);
}
