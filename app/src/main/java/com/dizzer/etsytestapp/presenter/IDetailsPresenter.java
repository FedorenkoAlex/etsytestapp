package com.dizzer.etsytestapp.presenter;

import com.dizzer.etsytestapp.view.contract.DetailsFragmentView;

public interface IDetailsPresenter {
    void setView(DetailsFragmentView view);

    void onSaveClick(Long id, String title, String price, String description,
                     String imageUrlFull, String imageUrlShort, boolean isSaved);

    void checkItem(Long id);
}
