package com.dizzer.etsytestapp.presenter.impl;

import android.app.Application;

import com.dizzer.etsytestapp.presenter.IMainPresenter;
import com.dizzer.etsytestapp.presenter.base.BasePresenter;
import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.contract.MainView;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

public class MainPresenter extends BasePresenter implements IMainPresenter {

    private String TAG = "MainPresenter";

    private MainView view;

    public MainPresenter(Retrofit retrofit, CompositeDisposable compositeDisposable, Application application) {
        super(retrofit, compositeDisposable, application);
    }

    @Override
    public void setView(MainView view) {
        this.view = view;
    }

    @Override
    public void addFragment(BaseFragment fragment) {
        view.setFragment(fragment);
    }
}
