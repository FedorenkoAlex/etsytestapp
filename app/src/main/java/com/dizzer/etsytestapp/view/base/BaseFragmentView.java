package com.dizzer.etsytestapp.view.base;


public interface BaseFragmentView {
    void showLoading();

    void hideLoading();

    void toastkMessage(String message);
}
