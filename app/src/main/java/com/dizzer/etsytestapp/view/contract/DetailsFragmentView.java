package com.dizzer.etsytestapp.view.contract;

import com.dizzer.etsytestapp.view.base.BaseFragmentView;

public interface DetailsFragmentView extends BaseFragmentView {
    void setIsSaved(boolean isSaved);
}
