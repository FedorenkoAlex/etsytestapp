package com.dizzer.etsytestapp.view.contract;

import android.support.v4.app.Fragment;

import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.view.base.BaseFragmentView;

import java.util.List;

public interface SavedFragmentView extends BaseFragmentView {
    void setData(List<ListingWithImageModel> data);

    void goToDetailsFragment(Fragment fragment);
}
