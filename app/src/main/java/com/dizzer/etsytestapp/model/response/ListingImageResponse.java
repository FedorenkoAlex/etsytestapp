package com.dizzer.etsytestapp.model.response;

import com.dizzer.etsytestapp.model.ListingImageModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListingImageResponse {
    @SerializedName("results")
    @Expose
    private List<ListingImageModel> results = null;

    public List<ListingImageModel> getResults() {
        return results;
    }

    public void setResults(List<ListingImageModel> results) {
        this.results = results;
    }
}
