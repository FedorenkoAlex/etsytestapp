package com.dizzer.etsytestapp.model.response;

import com.dizzer.etsytestapp.model.ListingModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListingResponse {
    @SerializedName("results")
    @Expose
    private List<ListingModel> results = null;

    public List<ListingModel> getResults() {
        return results;
    }

    public void setResults(List<ListingModel> results) {
        this.results = results;
    }
}
