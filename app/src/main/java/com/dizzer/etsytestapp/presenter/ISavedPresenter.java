package com.dizzer.etsytestapp.presenter;

import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.view.contract.SavedFragmentView;

public interface ISavedPresenter {
    void setView(SavedFragmentView view);

    void onStart();

    void onItemClicked(ListingWithImageModel item);
}
