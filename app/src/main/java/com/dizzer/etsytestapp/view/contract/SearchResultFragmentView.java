package com.dizzer.etsytestapp.view.contract;

import android.support.v4.app.Fragment;

import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.view.base.BaseFragmentView;

import java.util.List;

public interface SearchResultFragmentView extends BaseFragmentView {
    void showItems(List<ListingWithImageModel> items, boolean force);

    void showListingImage(ListingWithImageModel item);

    void goToDetailsFragment(Fragment fragment);
}
