package com.dizzer.etsytestapp.view;

import android.os.Bundle;

import com.dizzer.etsytestapp.R;
import com.dizzer.etsytestapp.presenter.impl.MainPresenter;
import com.dizzer.etsytestapp.view.base.BaseActivity;
import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.contract.MainView;
import com.dizzer.etsytestapp.view.fragment.MainScreenFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView {
    @Inject
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter.setView(this);
        presenter.addFragment(new MainScreenFragment());
    }

    @Override
    public void performDependencyInjection() {
        getActivityComponent().inject(this);
    }

    @Override
    public void setFragment(BaseFragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit();
    }
}
