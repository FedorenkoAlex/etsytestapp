package com.dizzer.etsytestapp.di.module;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.dizzer.etsytestapp.di.ActivityContext;
import com.dizzer.etsytestapp.di.PerActivity;
import com.dizzer.etsytestapp.presenter.impl.DetailsFragmentPresenter;
import com.dizzer.etsytestapp.presenter.impl.MainPresenter;
import com.dizzer.etsytestapp.presenter.impl.MainScreenFragmentPresenter;
import com.dizzer.etsytestapp.presenter.impl.SavedFragmentPresenter;
import com.dizzer.etsytestapp.presenter.impl.SearchFragmentPresenter;
import com.dizzer.etsytestapp.presenter.impl.SearchResultFragmentPresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

@Module
public class ActivityModule {
    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @PerActivity
    @Provides
    MainPresenter provideMainPresenter(Retrofit retrofit,
                                       CompositeDisposable compositeDisposable, Application application) {
        return new MainPresenter(retrofit, compositeDisposable, application);
    }

    @Provides
    SearchFragmentPresenter provideSearchFragmentPresenter(Retrofit retrofit,
                                                 CompositeDisposable compositeDisposable, Application application) {
        return new SearchFragmentPresenter(retrofit, compositeDisposable, application);
    }

    @Provides
    SearchResultFragmentPresenter provideSearchResultFragmentPresenter(Retrofit retrofit,
                                                                       CompositeDisposable compositeDisposable, Application application) {
        return new SearchResultFragmentPresenter(retrofit, compositeDisposable, application);
    }

    @Provides
    MainScreenFragmentPresenter provideMainScreenFragmentPresenter(Retrofit retrofit,
                                                               CompositeDisposable compositeDisposable, Application application) {
        return new MainScreenFragmentPresenter(retrofit, compositeDisposable, application);
    }

    @Provides
    DetailsFragmentPresenter provideDetailsFragmentPresenter(Retrofit retrofit,
                                                                CompositeDisposable compositeDisposable, Application application) {
        return new DetailsFragmentPresenter(retrofit, compositeDisposable, application);
    }

    @Provides
    SavedFragmentPresenter provideSavedFragmentPresenter(Retrofit retrofit,
                                                           CompositeDisposable compositeDisposable, Application application) {
        return new SavedFragmentPresenter(retrofit, compositeDisposable, application);
    }

}
