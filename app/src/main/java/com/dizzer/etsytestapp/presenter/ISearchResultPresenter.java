package com.dizzer.etsytestapp.presenter;

import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.view.contract.SearchResultFragmentView;

public interface ISearchResultPresenter {
    void setView(SearchResultFragmentView view);

    void loadListingImage(ListingWithImageModel item);

    void getListing(int offset, String query, String category);

    void onItemClicked(ListingWithImageModel item);
}
