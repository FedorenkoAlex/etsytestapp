package com.dizzer.etsytestapp.presenter.impl;

import android.app.Application;
import android.content.Context;

import com.dizzer.etsytestapp.model.CategoryModel;
import com.dizzer.etsytestapp.presenter.IMainScreenPresenter;
import com.dizzer.etsytestapp.presenter.base.BasePresenter;
import com.dizzer.etsytestapp.view.contract.MainScreenFragmentView;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

public class MainScreenFragmentPresenter extends BasePresenter implements IMainScreenPresenter {
    private String TAG = "MainScreenFragmentPresenter";

    private Context mContext;
    private List<CategoryModel> models;
    private MainScreenFragmentView view;

    public MainScreenFragmentPresenter(Retrofit retrofit, CompositeDisposable compositeDisposable, Application application) {
        super(retrofit, compositeDisposable, application);
        mContext = application.getApplicationContext();
    }

    @Override
    public void setView(MainScreenFragmentView view) {
        this.view = view;
    }
}
