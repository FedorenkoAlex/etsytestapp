package com.dizzer.etsytestapp.presenter.base;


import com.dizzer.etsytestapp.model.api.RestApi;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

public interface IBasePresenter {

    void onDestroy();

    Retrofit getRetrofit();

    RestApi getApiHelper();

    CompositeDisposable getCompositeDisposable();

    boolean isNetworkConnected();
}
