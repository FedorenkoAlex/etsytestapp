package com.dizzer.etsytestapp.view.contract;

import android.support.v4.app.Fragment;

import com.dizzer.etsytestapp.view.base.BaseFragmentView;

import java.util.List;

public interface SearchFragmentView extends BaseFragmentView {
    void setCategoryList(List<String> models);

    void openResultFragment(Fragment fragment);

    void showRetry();

    void hideRetry();
}
