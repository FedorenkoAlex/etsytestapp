package com.dizzer.etsytestapp.presenter.impl;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.dizzer.etsytestapp.db.Contract;
import com.dizzer.etsytestapp.model.CategoryModel;
import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.model.response.CategoryResponse;
import com.dizzer.etsytestapp.presenter.ISavedPresenter;
import com.dizzer.etsytestapp.presenter.ISearchPresenter;
import com.dizzer.etsytestapp.presenter.base.BasePresenter;
import com.dizzer.etsytestapp.utils.NetworkUtils;
import com.dizzer.etsytestapp.view.contract.SavedFragmentView;
import com.dizzer.etsytestapp.view.contract.SearchFragmentView;
import com.dizzer.etsytestapp.view.fragment.DetailsFragment;
import com.dizzer.etsytestapp.view.fragment.SearchResultFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SavedFragmentPresenter extends BasePresenter implements ISavedPresenter {
    private String TAG = "SavedFragmentPresenter";

    private List<ListingWithImageModel> list;
    private Context mContext;
    private SavedFragmentView view;

    public SavedFragmentPresenter(Retrofit retrofit, CompositeDisposable compositeDisposable, Application application) {
        super(retrofit, compositeDisposable, application);
        mContext = application.getApplicationContext();
        list = new ArrayList<>();
    }

    @Override
    public void setView(SavedFragmentView view) {
        this.view = view;
    }

    @Override
    public void onStart() {
        new DBLoader().execute();
    }

    @Override
    public void onItemClicked(ListingWithImageModel item) {
        view.goToDetailsFragment(DetailsFragment.newInstance(item.getListingId(), item.getTitle(),
                item.getPrice(), item.getDescription(), item.getImageFullUrl(), item.getImageSmallUrl(), false));
    }

    class DBLoader extends AsyncTask<Void, Void, List<ListingWithImageModel>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.showLoading();
        }

        @Override
        protected List<ListingWithImageModel> doInBackground(Void... voids) {
            List<ListingWithImageModel> listingWithImageModels = new ArrayList<>();

            Cursor cursor = mContext.getContentResolver().query(Contract.CONTENT_URI_FAVORITE,
                    null, null, null, null);

            if (cursor != null && cursor.moveToFirst()) {

                do {
                    ListingWithImageModel item = new ListingWithImageModel();
                    item.setListingId(cursor.getLong(cursor.getColumnIndex(Contract.FAVORITE_ID)));
                    item.setTitle(cursor.getString(cursor.getColumnIndex(Contract.FAVORITE_TITLE)));
                    item.setPrice(cursor.getString(cursor.getColumnIndex(Contract.FAVORITE_PRICE)));
                    item.setDescription(cursor.getString(cursor.getColumnIndex(Contract.FAVORITE_DESCRIPTION)));
                    item.setImageFullUrl(cursor.getString(cursor.getColumnIndex(Contract.FAVORITE_IMAGE_FULL)));
                    item.setImageSmallUrl(cursor.getString(cursor.getColumnIndex(Contract.FAVORITE_IMAGE_SHORT)));

                    listingWithImageModels.add(item);

                } while (cursor.moveToNext());

                cursor.close();
            }

            return listingWithImageModels;
        }

        @Override
        protected void onPostExecute(List<ListingWithImageModel> listingWithImageModels) {
            super.onPostExecute(listingWithImageModels);
            list = listingWithImageModels;
            view.hideLoading();
            view.setData(list);
        }
    }
}
