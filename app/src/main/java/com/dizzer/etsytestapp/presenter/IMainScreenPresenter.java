package com.dizzer.etsytestapp.presenter;

import com.dizzer.etsytestapp.view.contract.MainScreenFragmentView;

public interface IMainScreenPresenter {
    void setView(MainScreenFragmentView view);
}
