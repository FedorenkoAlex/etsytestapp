package com.dizzer.etsytestapp.presenter.impl;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.dizzer.etsytestapp.model.CategoryModel;
import com.dizzer.etsytestapp.model.response.CategoryResponse;
import com.dizzer.etsytestapp.presenter.ISearchPresenter;
import com.dizzer.etsytestapp.presenter.base.BasePresenter;
import com.dizzer.etsytestapp.utils.NetworkUtils;
import com.dizzer.etsytestapp.view.contract.SearchFragmentView;
import com.dizzer.etsytestapp.view.fragment.SearchResultFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SearchFragmentPresenter extends BasePresenter implements ISearchPresenter {
    private String TAG = "SearchFragmentPresenter";

    private Context mContext;
    private List<CategoryModel> models;
    private SearchFragmentView view;

    public SearchFragmentPresenter(Retrofit retrofit, CompositeDisposable compositeDisposable, Application application) {
        super(retrofit, compositeDisposable, application);
        mContext = application.getApplicationContext();
    }

    @Override
    public void setView(SearchFragmentView view) {
        this.view = view;
    }

    @Override
    public void onStart() {
        getCategory();
    }

    @Override
    public void onSubmitClick(String category, String query) {
        view.openResultFragment(SearchResultFragment.newInstance(category, query));
    }

    private void getCategory(){
        if (NetworkUtils.isNetworkConnected(mContext)) {
            view.hideRetry();
            view.showLoading();
            getCompositeDisposable().add(getApiHelper().getCategories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getCategoryData()));
        } else {
            view.toastkMessage("Please check internet connections");
            view.showRetry();
        }
    }

    private DisposableObserver<CategoryResponse> getCategoryData() {
        return new DisposableObserver<CategoryResponse>() {
            @Override
            public void onNext(CategoryResponse modelResponse) {
                if (modelResponse != null) {
                    models = modelResponse.getResults();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "getApps Error");
                view.toastkMessage("Problems with loading data");
                view.hideLoading();
            }

            @Override
            public void onComplete() {
                Log.i(TAG, "getApps Ok");
                onDateLoad();
            }
        };
    }

    private void onDateLoad() {
        createListOfCategoryTitle();
    }

    private void createListOfCategoryTitle(){
        List<String> categoryList = new ArrayList<>();
        for (CategoryModel i : models){
            categoryList.add(i.getLongName());
        }
        view.setCategoryList(categoryList);
        view.hideLoading();
    }
}
