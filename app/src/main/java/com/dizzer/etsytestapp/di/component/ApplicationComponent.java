package com.dizzer.etsytestapp.di.component;

import android.app.Application;
import android.content.Context;

import com.dizzer.etsytestapp.App;
import com.dizzer.etsytestapp.di.ApplicationContext;
import com.dizzer.etsytestapp.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(App app);

    @ApplicationContext
    Context context();

    Application application();

    Retrofit getRetrofit();
}
