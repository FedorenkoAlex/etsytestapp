package com.dizzer.etsytestapp.presenter.impl;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.dizzer.etsytestapp.model.ListingModel;
import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.model.response.ListingResponse;
import com.dizzer.etsytestapp.presenter.ISearchResultPresenter;
import com.dizzer.etsytestapp.presenter.base.BasePresenter;
import com.dizzer.etsytestapp.utils.NetworkUtils;
import com.dizzer.etsytestapp.view.contract.SearchResultFragmentView;
import com.dizzer.etsytestapp.view.fragment.DetailsFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SearchResultFragmentPresenter extends BasePresenter implements ISearchResultPresenter {
    private String TAG = "SearchResultFragmentPresenter";

    private Context mContext;
    private SearchResultFragmentView view;
    private List<ListingModel> listingModels;
    private List<ListingWithImageModel> listingWithImageModels;
    private ListingWithImageModel currentItem;
    private boolean force;


    public SearchResultFragmentPresenter(Retrofit retrofit, CompositeDisposable compositeDisposable, Application application) {
        super(retrofit, compositeDisposable, application);
        mContext = application.getApplicationContext();
        listingModels = new ArrayList<>();
        listingWithImageModels = new ArrayList<>();
    }

    @Override
    public void setView(SearchResultFragmentView view) {
        this.view = view;
    }

    @Override
    public void getListing(int offset, String query, String category) {
        if (NetworkUtils.isNetworkConnected(mContext)) {
            if (offset > 0) {
                force = true;
            } else {
                force = false;
            }
            view.showLoading();
            getCompositeDisposable().add(getApiHelper().getListings(category, query, 10, offset)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(getListingData()));
        } else {
            view.toastkMessage("Please check internet connections");
        }
    }

    @Override
    public void onItemClicked(ListingWithImageModel item) {
        view.goToDetailsFragment(DetailsFragment.newInstance(item.getListingId(), item.getTitle(),
                item.getPrice(), item.getDescription(), item.getImageFullUrl(), item.getImageSmallUrl(), false));
    }

    private DisposableObserver<ListingResponse> getListingData() {
        return new DisposableObserver<ListingResponse>() {
            @Override
            public void onNext(ListingResponse modelResponse) {
                if (modelResponse != null) {
                    listingModels = modelResponse.getResults();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "getListingData Error");
                view.toastkMessage("Problems with loading data");
                view.hideLoading();
            }

            @Override
            public void onComplete() {
                Log.i(TAG, "getListingData Ok");
                onDateLoad();
            }
        };
    }

    private void onDateLoad() {
        if (listingModels.size() == 0) {
            view.toastkMessage("No result");
            view.hideLoading();
        } else {
            creatingListingWithImage();
            view.hideLoading();
            view.showItems(listingWithImageModels, force);
        }
    }

    private void creatingListingWithImage() {
        for (ListingModel item : listingModels) {
            listingWithImageModels.add(convertListing(item));
        }
    }

    private ListingWithImageModel convertListing(ListingModel listingModel) {
        ListingWithImageModel result = new ListingWithImageModel();
        result.setListingId(listingModel.getListingId());
        result.setTitle(listingModel.getTitle());
        result.setPrice(listingModel.getPrice());
        result.setDescription(listingModel.getDescription());
        return result;
    }

    private Observable<ListingWithImageModel> getImageLink(ListingWithImageModel item) {
        return getApiHelper().getListingImages(item.getListingId())
                .map(listingImagesResponse -> {
                    item.setImageSmallUrl(listingImagesResponse.getResults().get(0).getUrl75x75());
                    item.setImageFullUrl(listingImagesResponse.getResults().get(0).getUrlFullxfull());
                    return item;
                });
    }

    @Override
    public void loadListingImage(ListingWithImageModel item) {
        view.showLoading();
        getCompositeDisposable().add(getImageLink(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getImageLinkData()));
    }

    private DisposableObserver<ListingWithImageModel> getImageLinkData() {
        return new DisposableObserver<ListingWithImageModel>() {
            @Override
            public void onNext(ListingWithImageModel modelResponse) {
                currentItem = modelResponse;
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "getListingData Error");
                view.toastkMessage("Problems with loading data");
                view.hideLoading();
            }

            @Override
            public void onComplete() {
                Log.i(TAG, "getListingData Ok");
                view.hideLoading();
                onImageLinkLoaded();
            }
        };
    }

    private void onImageLinkLoaded() {
        view.showListingImage(currentItem);
    }
}
