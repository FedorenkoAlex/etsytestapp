package com.dizzer.etsytestapp.presenter.impl;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.dizzer.etsytestapp.db.Contract;
import com.dizzer.etsytestapp.model.CategoryModel;
import com.dizzer.etsytestapp.model.response.CategoryResponse;
import com.dizzer.etsytestapp.presenter.IDetailsPresenter;
import com.dizzer.etsytestapp.presenter.ISearchPresenter;
import com.dizzer.etsytestapp.presenter.base.BasePresenter;
import com.dizzer.etsytestapp.utils.NetworkUtils;
import com.dizzer.etsytestapp.view.contract.DetailsFragmentView;
import com.dizzer.etsytestapp.view.contract.SearchFragmentView;
import com.dizzer.etsytestapp.view.fragment.SearchResultFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class DetailsFragmentPresenter extends BasePresenter implements IDetailsPresenter {
    private String TAG = "DetailsFragmentPresenter";

    private Context mContext;
    private DetailsFragmentView view;

    public DetailsFragmentPresenter(Retrofit retrofit, CompositeDisposable compositeDisposable, Application application) {
        super(retrofit, compositeDisposable, application);
        mContext = application.getApplicationContext();
    }

    @Override
    public void setView(DetailsFragmentView view) {
        this.view = view;
    }

    @Override
    public void onSaveClick(Long id, String title, String price, String description,
                            String imageUrlFull, String imageUrlShort, boolean isSaved) {
        if (isSaved) {
            addToDB(id, title, price, description, imageUrlFull, imageUrlShort);
        } else {
            deleteFromDB(id);
        }

    }

    private void addToDB(Long id, String title, String price, String description,
                         String imageUrlFull, String imageUrlShort) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Contract.FAVORITE_ID, id);
        contentValues.put(Contract.FAVORITE_TITLE, title);
        contentValues.put(Contract.FAVORITE_PRICE, price);
        contentValues.put(Contract.FAVORITE_DESCRIPTION, description);
        contentValues.put(Contract.FAVORITE_IMAGE_FULL, imageUrlFull);
        contentValues.put(Contract.FAVORITE_IMAGE_SHORT, imageUrlShort);
        mContext.getContentResolver().insert(Contract.CONTENT_URI_FAVORITE, contentValues);
        view.toastkMessage("Saved to favorite");
    }

    private void deleteFromDB(Long id) {
        mContext.getContentResolver().delete(Contract.CONTENT_URI_FAVORITE,
                Contract.FAVORITE_ID + " = " + id, null);
        view.toastkMessage("Delete from favorite");
    }

    @Override
    public void checkItem(Long id) {
        Cursor cursor = mContext.getContentResolver().query(Contract.CONTENT_URI_FAVORITE, null,
                Contract.FAVORITE_ID + " = " + id, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            view.setIsSaved(true);
            cursor.close();
        } else {
            view.setIsSaved(false);
        }
    }
}
