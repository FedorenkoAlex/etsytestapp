package com.dizzer.etsytestapp;

import android.app.Application;
import android.content.Context;

import com.dizzer.etsytestapp.di.component.ApplicationComponent;
import com.dizzer.etsytestapp.di.component.DaggerApplicationComponent;
import com.dizzer.etsytestapp.di.module.ApplicationModule;
import com.dizzer.etsytestapp.utils.AppConstants;

public class App extends Application {

    private static ApplicationComponent mApplicationComponent;

    public static Context getContext() {
        return mApplicationComponent.context();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this, AppConstants.BASE_URL)).build();

        mApplicationComponent.inject(this);
    }
}