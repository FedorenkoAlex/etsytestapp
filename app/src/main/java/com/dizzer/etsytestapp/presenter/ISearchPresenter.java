package com.dizzer.etsytestapp.presenter;

import com.dizzer.etsytestapp.view.contract.SearchFragmentView;

public interface ISearchPresenter {
    void setView(SearchFragmentView view);

    void onStart();

    void onSubmitClick(String category, String query);
}
