package com.dizzer.etsytestapp.presenter.base;

import android.app.Application;

import com.dizzer.etsytestapp.model.api.RestApi;
import com.dizzer.etsytestapp.utils.NetworkUtils;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

public class BasePresenter implements IBasePresenter {
    private final Retrofit mRetrofit;
    private final RestApi mApi;
    private final CompositeDisposable mCompositeDisposable;
    private final Application mApplication;

    public BasePresenter(Retrofit retrofit,
                         CompositeDisposable compositeDisposable, Application application) {
        this.mApplication = application;
        this.mRetrofit = retrofit;
        this.mCompositeDisposable = compositeDisposable;
        this.mApi = retrofit.create(RestApi.class);
    }

    public void onDestroy() {
        mCompositeDisposable.dispose();
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public RestApi getApiHelper() {
        return mApi;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(mApplication.getApplicationContext());
    }
}
