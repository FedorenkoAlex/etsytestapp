package com.dizzer.etsytestapp.view.base;

public interface BaseView {
    void showLoading();

    void hideLoading();

    void toastMessage(String message);
}
