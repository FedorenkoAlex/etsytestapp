package com.dizzer.etsytestapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dizzer.etsytestapp.R;
import com.dizzer.etsytestapp.adapter.ListingsRecyclerViewAdapter;
import com.dizzer.etsytestapp.di.component.ActivityComponent;
import com.dizzer.etsytestapp.model.ListingModel;
import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.presenter.impl.SearchResultFragmentPresenter;
import com.dizzer.etsytestapp.utils.AppConstants;
import com.dizzer.etsytestapp.view.base.BaseActivity;
import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.contract.SearchResultFragmentView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SearchResultFragment extends BaseFragment implements SearchResultFragmentView, ListingsRecyclerViewAdapter.OnListFragmentInteractionListener {

    Unbinder unbinder;
    @BindView(R.id.rvSearchResult)
    RecyclerView rvSearchResult;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private BaseActivity mActivity;
    private static final String SEARCH_CATEGORY = "category";
    private static final String SEARCH_QUERY = "query";
    private LinearLayoutManager mLayoutManager;
    private boolean isLoading = false;
    private final int VISIBLE_THRESHOLD = 1;
    private int lastVisibleItem, totalItemCount;
    private ListingsRecyclerViewAdapter adapter;
    private boolean adapterIsSet = false;

    private String query, category;

    @Inject
    SearchResultFragmentPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_result_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        performDependencyInjection();
        presenter.setView(this);
        mLayoutManager = new LinearLayoutManager(getContext());
        adapter = new ListingsRecyclerViewAdapter(this);
        refreshLayout.setOnRefreshListener(() -> loadFromOffset(0));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadFromOffset(0);
    }

    private void setRecyclerView(){
        rvSearchResult.setAdapter(adapter);
        rvSearchResult.setLayoutManager(mLayoutManager);
        rvSearchResult.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    isLoading = true;
                    rvSearchResult.post(() -> loadFromOffset(adapter.getItemCount()));
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getArgumentsData();
    }

    private void getArgumentsData() {
        if (getArguments() != null) {
            query = getArguments().getString(SEARCH_QUERY);
            category = getArguments().getString(SEARCH_CATEGORY);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.search_result_fragment;
    }

    @Override
    public void performDependencyInjection() {
        ActivityComponent component = getActivityComponent();
        if (getActivityComponent() != null) {
            component.inject(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
        }
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null) {
            return mActivity.getActivityComponent();
        }
        return null;
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }

    public static Fragment newInstance(String category, String query) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle args = new Bundle();
        args.putString(SEARCH_CATEGORY, category);
        args.putString(SEARCH_QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(ListingWithImageModel item) {
        presenter.onItemClicked(item);
    }

    @Override
    public void loadFromOffset(int offset) {
        presenter.getListing(offset, query, category);
    }

    @Override
    public void loadImageUrl(ListingWithImageModel item) {
        presenter.loadListingImage(item);
    }

    @Override
    public void showLoading() {
        isLoading = true;
        super.showLoading();
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        isLoading = false;
    }

    @Override
    public void showItems(List<ListingWithImageModel> items, boolean force) {
        if (!adapterIsSet){
            setRecyclerView();
        }
        if (force) {
            adapter.setItems(items);
        } else {
            adapter.addItems(items);
        }
        adapter.notifyDataSetChanged();
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
        rvSearchResult.setVisibility(View.VISIBLE);
    }

    @Override
    public void showListingImage(ListingWithImageModel item) {
        adapter.refreshItem(item);
    }

    @Override
    public void goToDetailsFragment(Fragment fragment) {
        FragmentTransaction ft = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, AppConstants.FRAGMENT_DETAILS)
                .commit();
        ft.addToBackStack(AppConstants.FRAGMENT_DETAILS);
    }
}
