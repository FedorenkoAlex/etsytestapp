package com.dizzer.etsytestapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.dizzer.etsytestapp.R;
import com.dizzer.etsytestapp.di.component.ActivityComponent;
import com.dizzer.etsytestapp.presenter.impl.SearchFragmentPresenter;
import com.dizzer.etsytestapp.utils.AppConstants;
import com.dizzer.etsytestapp.view.base.BaseActivity;
import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.contract.SearchFragmentView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SearchFragment extends BaseFragment implements SearchFragmentView {

    Unbinder unbinder;
    @BindView(R.id.searchFragmentEt)
    EditText searchFragmentEt;
    @BindView(R.id.searchFragmentSpinner)
    Spinner searchFragmentSpinner;
    @BindView(R.id.searchFragmentRetryBtn)
    Button searchFragmentRetryBtn;
    private BaseActivity mActivity;
    private ArrayAdapter<String> adapter;

    @Inject
    SearchFragmentPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        performDependencyInjection();
        adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item);
        presenter.setView(this);
        searchFragmentSpinner.setAdapter(adapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected int getLayout() {
        return R.layout.search_fragment;
    }

    @Override
    public void performDependencyInjection() {
        ActivityComponent component = getActivityComponent();
        if (getActivityComponent() != null) {
            component.inject(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
        }
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null) {
            return mActivity.getActivityComponent();
        }
        return null;
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }

    public static Fragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void setCategoryList(List<String> categoryList) {
        adapter.clear();
        adapter.addAll(categoryList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void openResultFragment(Fragment fragment) {
        FragmentTransaction ft = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, AppConstants.FRAGMENT_SEARCH_RESULT)
                .commit();
        ft.addToBackStack(AppConstants.FRAGMENT_SEARCH_RESULT);
    }

    @Override
    public void showRetry() {
        searchFragmentRetryBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        searchFragmentRetryBtn.setVisibility(View.GONE);
    }

    @OnClick({R.id.searchFragmentSubmitBtn, R.id.searchFragmentRetryBtn})
    void clickSubmit(View view) {
        switch (view.getId()){
            case R.id.searchFragmentSubmitBtn:
                String category = adapter.getItem(searchFragmentSpinner.getSelectedItemPosition());
                String query = searchFragmentEt.getText().toString();
                presenter.onSubmitClick(category, query);
                break;
            case R.id.searchFragmentRetryBtn:
              presenter.onStart();
        }
    }
}
