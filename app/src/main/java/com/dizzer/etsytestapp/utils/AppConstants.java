package com.dizzer.etsytestapp.utils;

public class AppConstants {
    public static final String BASE_URL = "https://openapi.etsy.com/v2/";
    public static final String QUERY_PARAMETER_KEY = "api_key";
    public static final String API_KEY = "rtia099r9s5uplsq6rr3uhyx";

    public static final String FRAGMENT_SEARCH_RESULT = "FRAGMENT_SEARCH_RESULT";
    public static final String FRAGMENT_DETAILS = "FRAGMENT_DETAILS";
    public static final String FRAGMENT_SAVED = "FRAGMENT_SAVED";

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}