package com.dizzer.etsytestapp.utils;


import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class EtsyInterceptor implements Interceptor {
    private String keyQuery = null;
    private String apiKey = null;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url()
                .newBuilder()
                .addQueryParameter(keyQuery, apiKey)
                .build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }

    public EtsyInterceptor setApiKey(String query, String apiKey) {
        this.keyQuery = query;
        this.apiKey = apiKey;
        return this;
    }
}