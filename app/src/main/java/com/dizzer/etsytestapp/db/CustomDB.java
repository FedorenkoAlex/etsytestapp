package com.dizzer.etsytestapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.dizzer.etsytestapp.db.Contract.FAVORITE_DESCRIPTION;
import static com.dizzer.etsytestapp.db.Contract.FAVORITE_ID;
import static com.dizzer.etsytestapp.db.Contract.FAVORITE_IMAGE_FULL;
import static com.dizzer.etsytestapp.db.Contract.FAVORITE_IMAGE_SHORT;
import static com.dizzer.etsytestapp.db.Contract.FAVORITE_PRICE;
import static com.dizzer.etsytestapp.db.Contract.FAVORITE_TITLE;
import static com.dizzer.etsytestapp.db.Contract.TABLE_NAME;

public class CustomDB extends SQLiteOpenHelper {

    private static final String DB_NAME = "Etsy.db";
    private static final int DB_VERSION = 1;

    private static final String SQL_CREATE_TABLE_FAVORITE = "CREATE TABLE " + TABLE_NAME
            + " ("
            + FAVORITE_ID + " INTEGER PRIMARY KEY,"
            + FAVORITE_TITLE + " TEXT,"
            + FAVORITE_PRICE + " TEXT,"
            + FAVORITE_DESCRIPTION + " TEXT,"
            + FAVORITE_IMAGE_FULL + " TEXT,"
            + FAVORITE_IMAGE_SHORT + " TEXT)";

    private static final String SQL_DROP_TABLE_IMAGE = "DROP TABLE " + TABLE_NAME;

    public CustomDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_FAVORITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_IMAGE);
        onCreate(sqLiteDatabase);
    }
}