package com.dizzer.etsytestapp.model.api;


import com.dizzer.etsytestapp.model.response.CategoryResponse;
import com.dizzer.etsytestapp.model.response.ListingImageResponse;
import com.dizzer.etsytestapp.model.response.ListingResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestApi {
    @GET("taxonomy/categories")
    Observable<CategoryResponse> getCategories();

    @GET("listings/active")
    Observable<ListingResponse> getListings(@Query("category") String category,
                                                  @Query("keywords") String keywords,
                                                  @Query("limit") int limit,
                                                  @Query("offset") int offset);

    @GET("listings/{listing_id}/images")
    Observable<ListingImageResponse> getListingImages(@Path("listing_id") Long listingId);
}
