package com.dizzer.etsytestapp.db;

import android.net.Uri;

public class Contract {

    static final String TABLE_NAME = "favorite";

    public static final String FAVORITE_ID = "_id";
    public static final String FAVORITE_TITLE = "title";
    public static final String FAVORITE_PRICE = "price";
    public static final String FAVORITE_DESCRIPTION = "description";
    public static final String FAVORITE_IMAGE_FULL = "image_full";
    public static final String FAVORITE_IMAGE_SHORT = "image_short";

    static final String CONTENT_AUTHORITY = "com.dizzer.etsytestapp.db";
    static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    static final String PATH = "images";
    public static final Uri CONTENT_URI_FAVORITE = BASE_CONTENT_URI.buildUpon().appendPath(PATH).build();
}
