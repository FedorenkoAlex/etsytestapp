package com.dizzer.etsytestapp.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dizzer.etsytestapp.R;
import com.dizzer.etsytestapp.adapter.ListingsRecyclerViewAdapter;
import com.dizzer.etsytestapp.di.component.ActivityComponent;
import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.dizzer.etsytestapp.presenter.impl.SavedFragmentPresenter;
import com.dizzer.etsytestapp.utils.AppConstants;
import com.dizzer.etsytestapp.view.base.BaseActivity;
import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.contract.SavedFragmentView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SavedFragment extends BaseFragment implements SavedFragmentView, ListingsRecyclerViewAdapter.OnListFragmentInteractionListener {

    Unbinder unbinder;
    @BindView(R.id.rvSavedFragment)
    RecyclerView rvSavedResult;
    private BaseActivity mActivity;
    private LinearLayoutManager mLayoutManager;
    private ListingsRecyclerViewAdapter adapter;

    @Inject
    SavedFragmentPresenter presenter;

    public static Fragment newInstance() {
        SavedFragment fragment = new SavedFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.saved_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        performDependencyInjection();
        presenter.setView(this);
        mLayoutManager = new LinearLayoutManager(getContext());
        adapter = new ListingsRecyclerViewAdapter(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    private void setRecyclerView() {
        rvSavedResult.setAdapter(adapter);
        rvSavedResult.setLayoutManager(mLayoutManager);
    }

    @Override
    protected int getLayout() {
        return R.layout.saved_fragment;
    }

    @Override
    public void performDependencyInjection() {
        ActivityComponent component = getActivityComponent();
        if (getActivityComponent() != null) {
            component.inject(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
        }
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null) {
            return mActivity.getActivityComponent();
        }
        return null;
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }

    @Override
    public void onClick(ListingWithImageModel item) {
        presenter.onItemClicked(item);
    }

    @Override
    public void goToDetailsFragment(Fragment fragment) {
        FragmentTransaction ft = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, AppConstants.FRAGMENT_DETAILS)
                .commit();
        ft.addToBackStack(AppConstants.FRAGMENT_DETAILS);
    }

    @Override
    public void loadFromOffset(int offset) {
        //load local data
    }

    @Override
    public void loadImageUrl(ListingWithImageModel item) {
        //load local data
    }

    @Override
    public void setData(List<ListingWithImageModel> data) {
        adapter.setList(data);
        setRecyclerView();
    }
}
