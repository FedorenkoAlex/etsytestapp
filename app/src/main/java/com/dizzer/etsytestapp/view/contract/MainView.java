package com.dizzer.etsytestapp.view.contract;

import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.base.BaseView;

public interface MainView extends BaseView {
    void setFragment(BaseFragment fragment);
}
