package com.dizzer.etsytestapp.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.text.HtmlCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dizzer.etsytestapp.R;
import com.dizzer.etsytestapp.model.ListingWithImageModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListingsRecyclerViewAdapter extends RecyclerView.Adapter<ListingsRecyclerViewAdapter.ViewHolder> {
    private OnListFragmentInteractionListener mListener;
    private List<ListingWithImageModel> list;

    public interface OnListFragmentInteractionListener {
        void onClick(ListingWithImageModel item);

        void loadFromOffset(int offset);

        void loadImageUrl(ListingWithImageModel item);
    }

    public ListingsRecyclerViewAdapter(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        list = new ArrayList<>();
    }

    public List<ListingWithImageModel> getList() {
        return list;
    }

    public void setList(List<ListingWithImageModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (list.get(i).getImageSmallUrl() == null) {
            if (null != mListener) {
                mListener.loadImageUrl(list.get(i));
            }
            viewHolder.rvImage.setImageDrawable(null);
        } else {
            Picasso.get()
                    .load(list.get(i).getImageSmallUrl())
                    .into(viewHolder.rvImage);
        }
        viewHolder.rvItemTitle.setText(HtmlCompat.fromHtml(list.get(i).getTitle(),
                HtmlCompat.FROM_HTML_MODE_LEGACY));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void refreshItem(ListingWithImageModel item) {
        int id = list.indexOf(item);
        if (id != -1) {
            list.set(id, item);
            notifyItemChanged(id);
        }
    }

    public void addItems(List<ListingWithImageModel> items) {
        list.addAll(items);
    }

    public void setItems(List<ListingWithImageModel> items) {
        list.clear();
        addItems(items);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.rvImage)
        ImageView rvImage;
        @Nullable
        @BindView(R.id.rvItemTitle)
        TextView rvItemTitle;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(l -> mListener.onClick(list.get(getLayoutPosition())));
        }
    }
}
