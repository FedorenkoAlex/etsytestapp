package com.dizzer.etsytestapp.presenter;

import com.dizzer.etsytestapp.presenter.base.IBasePresenter;
import com.dizzer.etsytestapp.view.base.BaseFragment;
import com.dizzer.etsytestapp.view.contract.MainView;

public interface IMainPresenter extends IBasePresenter {
    void setView(MainView view);

    void addFragment(BaseFragment fragment);
}
