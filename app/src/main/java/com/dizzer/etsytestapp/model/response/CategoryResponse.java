package com.dizzer.etsytestapp.model.response;

import com.dizzer.etsytestapp.model.CategoryModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryResponse {
    @SerializedName("count")
    @Expose
    private Long count;
    @SerializedName("results")
    @Expose
    private List<CategoryModel> results = null;
    @SerializedName("params")
    @Expose
    private Object params;
    @SerializedName("type")
    @Expose
    private String type;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<CategoryModel> getResults() {
        return results;
    }

    public void setResults(List<CategoryModel> results) {
        this.results = results;
    }

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
